# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script contains a sequence of calls that are needed for running a test

if [[ -n "$FUEGO_DEBUG" && $(( $FUEGO_DEBUG & 1 )) = 1 ]] ; then
    set -x
fi

source $FUEGO_CORE/scripts/overlays.sh
set_overlay_vars
source $FUEGO_CORE/scripts/functions.sh
source $TEST_HOME/fuego_test.sh

# support specification of individual test phases
# in general, it is dangerous to do a phase without also doing
# the precursors to that phase, but use at your own risk
if [ -z "$FUEGO_TEST_PHASES" ] ; then
    export FUEGO_TEST_PHASES="pre_test pre_check build deploy snapshot run post_test processing"
fi


# set_phase: prepare for executing a particular phase
# $1 is the phase name
function set_phase {
    FUEGO_CUR_PHASE=$1
    set_loglevel $FUEGO_CUR_PHASE
    iprint "===== doing fuego phase: $FUEGO_CUR_PHASE ====="
}

# Show environment variables if we're debugging.
# There are a lot of them
if [ -n "$FUEGO_LL_DEBUG" ] ; then
    echo "Test environment variables:"
    env | sort
fi

if [[ "$FUEGO_TEST_PHASES" == *pre_test* ]] ; then
    set_phase pre_test
    # FIXTHIS: do not require board connection too early
    pre_test
else
    true
fi

if [[ "$FUEGO_TEST_PHASES" == *build* ]] ; then
    set_phase build
    build
fi


if [[ "$FUEGO_TEST_PHASES" == *deploy* ]] ; then
    set_phase deploy
    deploy
fi

if [[ "$FUEGO_TEST_PHASES" == *makepkg* ]] ; then
    set_phase makepkg
    makepkg
fi

if [[ "$FUEGO_TEST_PHASES" == *snapshot* ]] ; then
    set_phase snapshot
    snapshot
fi

if [[ "$FUEGO_TEST_PHASES" == *run* ]] ; then
    set_phase run
    echo "-------------------------------------------------"
    call_if_present test_run
    echo "-------------------------------------------------"
fi

if [[ "$FUEGO_TEST_PHASES" == *post_test* ]] ; then
    set_phase post_test
    post_test
else
    # at least reset the signal handling
    trap post_term_handler SIGTERM
    trap - SIGHUP SIGALRM SIGINT ERR EXIT
fi

RETURN_VALUE=0
if [[ "$FUEGO_TEST_PHASES" == *processing* ]] ; then
    set_phase processing
    processing
fi

iprint "Fuego: requested test phases complete!"
exit $RETURN_VALUE
