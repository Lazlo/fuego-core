#!/bin/bash
#
# poll_requests.sh - this is a simple script to do an infinite loop
# polling for requests (for my lab) on the fuego server. If a job
# is found, it is executed.
#

# wait time between polling, in seconds
WAIT_TIME=60

# remove requests from server (1=remove, 0=don't remove)
REMOVE_REQUESTS=0

# if BOARD is set, only process requests for the indicated board
#BOARD="min1"
if [ -n "${BOARD}" ] ; then
    BOARD_ARG="board=$BOARD"
else
    BOARD_ARG=""
fi


server=$(ftc config fuego_server)
echo "Polling $server for requests"
echo "Type Ctrl-C to exit"

# could do 'read -rsn1' inside the loop, but this won't
# trigger during the sleep

while true ; do
    echo -n "Checking "
    request_id="$(ftc list-requests -q ${BOARD_ARG} state=pending | head -n 1)"
    if [ -n "${request_id}" ] ; then
        echo
        echo "Running request: $request_id"
        ftc run-request --put-run $request_id ;
        if [ "$REMOVE_REQUESTS" = "1" ] ; then
            ftc rm-request $request_id ;
        fi
    else
        echo -n "Waiting "
        for i in $(seq $WAIT_TIME) ; do
            echo -n "."
            sleep 1
        done
        echo
    fi
done
