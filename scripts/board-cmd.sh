#!/bin/bash
#
# board-cmd.sh - execute a command on a board, without invoking a test
#
# usage: board-cmd.sh <board> <command>
#
# Note: uses ftc query-board to get the variables and functions
# required to execute a command on the target

function usage() {
	echo "Usage: board-cmd.sh <board> \"<command>\""
	exit $1
}

function abort_job() {
	echo "*** ABORTED: reason: $1"
	exit 1
}

# define some utility functions used by some routines
function assert_define() {
	if [ -z "${!1}" ] ; then
		abort_job "$1 variable is required but not defined"
	fi
}

board=$1
if [ -z "$board" ] ; then
	echo "Error: Missing board"
	usage 1
fi
shift
if [ -z "$1" ] ; then
	echo "Error: Missing command"
	usage 1
fi

# uncomment this line to debug the script
#set -x

# get the board variables, including the ov_transport_cmd function
tmpfile=$(mktemp --tmpdir fuego-$board-XXXXXXXX)
if ftc query-board -b $board --sh >$tmpfile ; then
    source $tmpfile
    #rm ${tmpfile}

    ov_transport_cmd "$@"
fi

