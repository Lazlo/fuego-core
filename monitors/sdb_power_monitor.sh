#!/bin/bash
#
# Fuego monitor - sdb_power_monitor
#
# this is run in the context of a test run
# It may use the following:
#  TRANSPORT
#  NODE_NAME
#  BOARD_CONTROL
#  POWER_MONITOR_TYPE

control_dev=$(ttc $NODE_NAME info -n control_dev)
if [ -z "$control_dev" ] ; then
    echo "ERROR: Can not get control device for Sony Debug Board for $NODE_NAME"
    echo "Monitor not started"
    exit 1
fi

grabserial -d $control_dev -t | grep -i VBUS=

