#!/bin/sh

#  In target, run command wall.

test="wall"

if wall "Testing wall command"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
