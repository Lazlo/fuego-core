#!/bin/sh

#  In the target start acpid, and confirm the process condition by command ps.
#  check the keyword "acpid".

test="ps"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target acpid stop
if exec_service_on_target acpid start
then
    echo " -> start of acpid succeeded."
else
    echo " -> start of acpid failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if [ -z "$(ps aux | grep acpid | grep "/usr/sbin/acpid" | grep -v grep)" ]
then
    echo " -> can't get the pid of acpid."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target acpid stop
then
    echo " -> stop of acpid succeeded."
else
    echo " -> stop of acpid failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps aux | grep acpid | grep "/usr/sbin/acpid" | grep -v grep
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi
