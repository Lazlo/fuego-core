#!/bin/sh

#  In target, run command vconfig.
#  option: add/rem

test="addaddr"

vconfig add $vcon_ifeth 100
ifconfig $vcon_ifeth.100 inet 192.168.255.1 netmask 0xffffff00

if ifconfig $vcon_ifeth.100 | grep "192.168.255.1"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
vconfig rem $vcon_ifeth.100
