#!/bin/sh
# board_status.sh - dump lots of information from the target
# support for the following is assumed (and mandatory)
# echo, shell functions, command line redirection

check_for_program() {
    command -v "$1" >/dev/null 2>&1
}

check_and_run() {
    if check_for_program $1 ; then
        $@
    else
        echo "  ($1 command not available)"
    fi
}

set +e

RESULT="SUCCESS"
echo "====================================="
echo "== Fuego board status scan results =="
echo "Information:"
echo "  account information: $(id)"
echo "  working directory: $(pwd)"
echo "  kernel version: $(uname -a)"
echo "  PATH: $PATH"
echo "  os-release:"
cat /etc/os-release
echo "====================================="
echo "Scan results:"

echo "-------------------------------------"
echo "/proc/cpuinfo:"
cat /proc/cpuinfo

echo "-------------------------------------"
echo "/proc/meminfo:"
cat /proc/meminfo

echo "-------------------------------------"
echo "modules:"
if check_for_program lsmod ; then
    lsmod
else
    # output format not as pleasant, but mostly same info
    cat /proc/modules
fi

echo "-------------------------------------"
echo "USB devices: (lsusb)"
if check_for_program lsusb ; then
    lsusb -v
else
    # poor man's lsusb
    for d in $(find /sys/bus/usb/devices) ; do
        echo "--------"
        echo "USB device: $d"
        for f in $d/* ; do
            if [ -e $f ] ; then
                echo -n "$f:"
                cat $f
            fi
        done
    done
fi

echo "-------------------------------------"
echo "blkid:"
check_and_run blkid

echo "-------------------------------------"
echo "mount:"
mount

echo "-------------------------------------"
echo "df:"
df

echo "-------------------------------------"
echo "Network devices: (ifconfig)"
if check_for_program ifconfig ; then
    ifconfig
else
    echo "  route:"
    cat /proc/net/route
    echo "  devs:"
    cat /proc/net/dev
fi

echo "-------------------------------------"
echo "Process 1 is program: $(cat /proc/1/cmdline)"

echo "-------------------------------------"
echo "ps:"
# select between full-featured ps and busybox (minimal) ps
if ! ps aux 2>/dev/null ; then
    ps
fi

echo "-------------------------------------"
echo "dmesg:"
check_and_run dmesg

echo "-------------------------------------"
echo "Packages: (dpkg)"
check_and_run dpkg -l

echo "-------------------------------------"
echo "RESULT=$RESULT"
echo "-------------------------------------"

