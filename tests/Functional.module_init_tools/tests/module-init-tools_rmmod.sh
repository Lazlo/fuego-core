#!/bin/sh

#  In target, run command rmmod.
#  option: none

test="rmmod"

tst_mod_file="ko file of module"
tst_mod_flag=0

if modinfo $MODULE_NAME
then
    tst_mod_file=$(modinfo $MODULE_NAME | sed -n 1p | cut -d' ' -f8)
else
    echo " -> module $MODULE_NAME does not exist."
    echo " -> $test: TEST-FAIL"
    exit
fi

mkdir test_dir
cp $tst_mod_file test_dir/

if lsmod | grep $MODULE_NAME
then
    tst_mod_flag=1
    rmmod $tst_mod_file
else
    insmod test_dir/*.ko
    rmmod $tst_mod_file
fi


if lsmod | grep $MODULE_NAME
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

if [ $tst_mod_flag=1 ]
then
    insmod $tst_mod_file
fi
rm -fr test_dir
