NEED_ROOT=1
MODULE_NAME=$FUNCTIONAL_MODULE_INIT_TOOLS_TEST_MODULE_NAME

function test_pre_check {
    assert_define MODULE_NAME "Missing name of module to test with. See spec.json file and define test_module_name."
    assert_has_program depmod
    assert_has_program insmod
    assert_has_program lsmod
    assert_has_program rmmod
    assert_has_program modinfo
    assert_has_program modprobe
    cmd "modinfo $MODULE_NAME >/dev/null" || abort_job "Missing $MODULE_NAME.ko kernel module"
}

function test_deploy {
    put $TEST_HOME/module-init-tools_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $FUEGO_CORE/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    ./module-init-tools_test.sh $MODULE_NAME"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
