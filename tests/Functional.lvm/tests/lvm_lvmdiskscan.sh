#!/bin/sh

#  In target, scan for all devices visible to LVM2.
#  option: none

test="lvmdiskscan"

if lvmdiskscan | grep "partitions"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
