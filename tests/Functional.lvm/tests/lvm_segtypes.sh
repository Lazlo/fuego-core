#!/bin/sh

#  In target, display recognised Logical Volume segment types.
#  option: none

test="segtypes"

if lvm segtypes
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
