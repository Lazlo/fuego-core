#!/bin/sh

#  In target, display recognised metadata formats.
#  option: none

test="formats"

if lvm formats | grep "lvm"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
