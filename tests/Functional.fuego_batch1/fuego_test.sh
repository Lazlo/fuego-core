# test to experiment with a different form of batch jobs
# right now, Fuego uses testplans for this
# this attempts to do run multiple tests as nested tests
# from this batch test.
#
# Notes:
# - each sub-test is a testcase
# - we use TAP output to summarize the results for this test
#

function test_pre_check {
    assert_define FUNCTIONAL_FUEGO_BATCH1_TESTS
}

function test_run {
    # work around a bug with nested invocations.  The nested calls
    # to 'ftc run-test' will wipe out the 'before' system log.
    # Save it off before that happens, and restore it after
    # the nested tests.
    local fuego_test_tmp="${FUEGO_TARGET_TMP:-/tmp}/fuego.$TESTDIR"
    local syslog_before="${fuego_test_tmp}/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.before"

    # NOTE: this approach doesn't handle test concurrency very well
    # multiple tests running nested could overwrite syslog_before.save
    # But I don't care at the moment.
    cmd "cp ${syslog_before} /tmp/syslog_before.save"

    export FUEGO_CALLER=nested
    echo "FUNCTIONAL_FUEGO_BATCH1_TESTS=\"${FUNCTIONAL_FUEGO_BATCH1_TESTS}\""
    #export FUEGO_BATCH_ID
    set +e
    tc_num=1
    for TEST_NAME in $FUNCTIONAL_FUEGO_BATCH1_TESTS ; do
        echo "TEST_NAME is $TEST_NAME"
        ftc run-test -b $NODE_NAME -t $TEST_NAME
        result=$?
        if [ $result == "0" ] ; then
            log_this "echo \"ok $tc_num $TEST_NAME\""
        else
            log_this "echo \"not ok $tc_num $TEST_NAME\""
        fi
        tc_num=$(( tc_num + 1 ))
    done
    set -e

    # re-create target log dir, and restore the 'before' syslog dump
    cmd "mkdir -p ${fuego_test_tmp}"
    cmd "mv -f /tmp/syslog_before.save ${syslog_before}"
}

function test_processing {
    log_compare "$TESTDIR" "0" "not ok" "n"
}
