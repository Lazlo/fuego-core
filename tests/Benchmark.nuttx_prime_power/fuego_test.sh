# for a built-in test, make sure the test is configured properly
# to be built into the kernel, and then build the kernel
function test_build {
    ttc $NODE_NAME get_kernel
    pushd spresense/sdk
    ttc $NODE_NAME get_config
    tools/config.py examples/prime
    # make kernel_build
    # make
    ttc $NODE_NAME kbuild
    popd
}

# put nuttx.spk on the board
function test_deploy {
    pushd spresense/sdk
    ttc $NODE_NAME kinstall
    popd
    # use a stub command to get past the welcome prompt
    # (after the reboot from the kinstall)
    ttc $NODE_NAME run "true"
}

function test_run {
    start_monitor power_monitor
    cmd prime
    stop_monitor power_monitor
}

function test_processing {
    log_compare "$TESTDIR" "1" "Time consumed:" "p"
}
