tarball=ipv6connect.tar.gz

function test_build {
    make CC="$CC" LD="$LD"
}

function test_deploy {
    put ipv6connect  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./ipv6connect"
}
