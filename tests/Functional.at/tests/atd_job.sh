#!/bin/sh

#  In the target start atd, check the operation of add, list and remove job.

test="job"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target atd stop

if exec_service_on_target atd start
then
    echo " -> start atd succeeded."
else
    echo " -> start atd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

mkdir test_dir
cp data/test_add.sh test_dir/

if at -f test_dir/test_add.sh now + 5 minutes
then
    echo " -> add job succeeded."
else
    echo " -> add job failed."
    echo " -> $test: TEST-FAIL"
    rm -fr test_dir
    exec_service_on_target atd stop
    exit
fi

if at -c $(at -l | cut -b 1-2) | grep "fuego autotest"
then
    echo " -> cats the jobs listed on the command line succeeded."
else
    echo " -> cats the jobs listed on the command line failed."
    echo " -> $test: TEST-FAIL"
    rm -fr test_dir
    exec_service_on_target atd stop
    exit
fi

if at -l
then
    echo " -> list job succeeded."
else
    echo " -> list job failed."
    echo " -> $test: TEST-FAIL"
    rm -fr test_dir
    exec_service_on_target atd stop
    exit
fi

rm -fr test_dir

if at -d $(at -l | cut -b 1-2)
then
    echo " -> $test: TEST-PASS"
else
    echo " -> remove job failed."
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target atd stop
