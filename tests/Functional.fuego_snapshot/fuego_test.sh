# test that the machine_status file exists, and that it
# contains data from our custom test_snapshot function.

function test_snapshot {
    echo "============================================================" >>$SNAPSHOT_FILENAME
    echo "Board state information (custom snapshot):" >>$SNAPSHOT_FILENAME
    ov_rootfs_state >>$SNAPSHOT_FILENAME
    echo "network_devices_status:" >>$SNAPSHOT_FILENAME
    cmd "cat /proc/net/dev" >>$SNAPSHOT_FILENAME
    return 0
}

function test_run {
    log_this "if [ ! -f $LOGDIR/machine-snapshot.txt ] ; then \
	    echo \"FAILURE - missing machine-snapshot file\" ; \
	fi ; \
        if grep network_devices_status $LOGDIR/machine-snapshot.txt >/dev/null ; then \
	    echo SUCCESS ; \
        else \
	    echo \"FAILURE - snapshot file is missing expected data\" ; \
        fi"
}

function test_processing {
    log_compare "$TESTDIR" "1" "SUCCESS" "p"
}
