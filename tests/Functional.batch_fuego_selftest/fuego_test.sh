# test to test bc with different specs
#
# Notes:
# - each sub-test is a testcase
# - we use TAP output to summarize the results for this test
#

BATCH_TESTPLAN=$(cat <<END_TESTPLAN
{
    "testPlanName": "fuego_test",
    "comment": "This has the self-tests that are used for release testing",
    "comment2": "Tests are listed alphabetically, after the board check",
    "default_timeout": "3m",
    "default_spec": "default",
    "tests": [
        { "comment": "Start with a board check first",
            "testName": "Functional.fuego_board_check" },
        { "testName": "Functional.bc" },
        { "testName": "Benchmark.fuego_check_plots",
          "extralinks": {"plot": "plot.png"} },
        { "testName": "Functional.fuego_abort" },
        { "testName": "Functional.fuego_board_status", "timeout": "6m" },
        { "testName": "Functional.fuego_check_tables" },
        { "testName": "Functional.fuego_common_check" },
        { "testName": "Functional.fuego_compare_reports" },
        { "testName": "Functional.fuego_dependencies" },
        { "testName": "Functional.fuego_ftc_check" },
        { "testName": "Functional.fuego_ftc_test" },
        { "testName": "Functional.fuego_lint" },
        { "testName": "Functional.fuego_release_test", "timeout": "30m" },
        { "testName": "Functional.fuego_report_live" },
        { "testName": "Functional.fuego_sleep", "timeout": "7m" },
        { "testName": "Functional.fuego_slow_log" },
        { "testName": "Functional.fuego_snapshot" },
        { "testName": "Functional.fuego_test_minimal" },
        { "testName": "Functional.fuego_test_phases" },
        { "testName": "Functional.fuego_test_variables" },
        { "testName": "Functional.fuego_tguid_check" },
        { "testName": "Functional.fuego_transport", "timeout": "10m" },
        { "testName": "Functional.hello_world" },
        { "testName": "Functional.hello_world", "spec": "hello-fail", "timeout": "3m" },
        { "testName": "Benchmark.Dhrystone", "spec": "100M" }
    ]
}
END_TESTPLAN
)

function test_run {
    export TC_NUM=1
    DEFAULT_TIMEOUT=3m
    export FUEGO_BATCH_ID="st-$(allocate_next_batch_id)"

    # don't stop on test errors
    set +e
    log_this "echo \"batch_id=$FUEGO_BATCH_ID\""
    run_test Functional.fuego_board_check
    run_test Functional.bc
    run_test Benchmark.fuego_check_plots
    run_test Functional.fuego_abort
    run_test Functional.fuego_board_status --timeout 6m
    run_test Functional.fuego_check_tables
    run_test Functional.fuego_common_check
    run_test Functional.fuego_compare_reports
    run_test Functional.fuego_dependencies
    run_test Functional.fuego_ftc_check
    run_test Functional.fuego_ftc_test
    run_test Functional.fuego_lint
    run_test Functional.fuego_release_test --timeout 30m
    run_test Functional.fuego_report_live
    run_test Functional.fuego_sleep --timeout 7m
    run_test Functional.fuego_slow_log
    run_test Functional.fuego_snapshot
    run_test Functional.fuego_test_minimal
    run_test Functional.fuego_test_phases
    run_test Functional.fuego_test_variables
    run_test Functional.fuego_tguid_check
    run_test Functional.fuego_transport --timeout 10m
    run_test Functional.hello_world
    run_test Functional.hello_world -s hello-fail --timeout 3m
    run_test Benchmark.Dhrystone -s 100
    set -e
}
