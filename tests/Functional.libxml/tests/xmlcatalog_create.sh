#!/bin/sh

#  In the target to execute command xmlcatalog and confirm the result.
#  option : --create

test="xmlcatalog2"

if xmlcatalog --create | grep "DTD"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
