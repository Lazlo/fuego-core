#!/bin/sh

#  Check the help message of command openct-control.

test="help"

if openct-control -h | grep "usage"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
