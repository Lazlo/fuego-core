#!/bin/sh

#  In the target start bgpd and zebra, then confirm the log file.
#  check the /var/log/quagga/bgpd.log file.

test="logfile"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target bgpd stop
exec_service_on_target zebra stop

if [ ! -f /var/log/quagga/bgpd.log ]
then
   mkdir /var/log/quagga
   chown -R quagga:quagga /var/log/quagga
fi

mv /var/log/quagga/bgpd.log /var/log/quagga/bgpd.log.bck

#Backup the config file
mv /etc/quagga/bgpd.conf /etc/quagga/bgpd.conf.bck
mv /etc/quagga/zebra.conf /etc/quagga/zebra.conf.bck

cp data/bgpd.conf /etc/quagga/bgpd.conf
cp data/zebra.conf /etc/quagga/zebra.conf
chown quagga:quagga /etc/quagga/*.conf

if exec_service_on_target zebra start
then
    echo " -> start of zebra succeeded."
else
    echo " -> start of zebra failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target bgpd start
then
    echo " -> start of bgpd succeeded."
else
    echo " -> start of bgpd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ls /var/log/quagga/bgpd.log
then
    echo " -> get log file of bgpd."
    echo " -> $test: TEST-PASS"
else
    echo " -> can't get log file of bgpd."
    echo " -> $test: TEST-FAIL"
    exit
fi

exec_service_on_target bgpd stop
exec_service_on_target zebra stop

#Restore the config file
mv /etc/quagga/bgpd.conf.bck /etc/quagga/bgpd.conf
mv /etc/quagga/zebra.conf.bck /etc/quagga/zebra.conf

if [ -f /var/log/quagga/bgpd.log.bck ]
then
    mv /var/log/quagga/bgpd.log.bck /var/log/quagga/bgpd.log
fi
