#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys, collections
import common as plib

results = {}
results = collections.OrderedDict()

regex_string = '^(ok|not ok) (\d+) (.*)$'
matches = plib.parse_log(regex_string)

for m in matches:
    print("DEBUG: in parser.py: m=%s" % str(m))
    status = m[0]
    test_num = m[1]
    test_id = m[2].replace(" ",".")
    results[test_id] = 'PASS' if status == 'ok' else 'FAIL'

# split the output for each testcase
# information follows result line for each test (info_follows_regex=1)
plib.split_output_per_testcase(regex_string, results, 1)

sys.exit(plib.process(results))
