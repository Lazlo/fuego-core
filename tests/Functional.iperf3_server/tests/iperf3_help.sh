#!/bin/sh

#  Test command iperf3 on target.
#  Option : --help

test="help"

if iperf3 --help | grep "Usage"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
