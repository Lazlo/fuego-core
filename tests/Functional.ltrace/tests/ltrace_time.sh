#!/bin/sh

#  In target, run comannd ltrace to show the time spent inside each call.
#  option: -T

test="time"

rm log_for_fuego || true
ltrace -T ls > log_for_fuego 2>&1
if grep "<[0-9].[0-9]\{6\}>" log_for_fuego
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm log_for_fuego
