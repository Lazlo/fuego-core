#!/bin/sh

# Start the named on target.
# Check the log of named.

test="named_syslog"

logger_service=$(detect_logger_service)

named_status=$(get_service_status named)
dnsmasq_status=$(get_service_status dnsmasq)
exec_service_on_target dnsmasq stop

if [ -f /etc/bind/rndc.key ]
then
    rm -f /etc/bind/rndc.key
fi

exec_service_on_target named stop
exec_service_on_target syslog.socket stop
exec_service_on_target $logger_service stop

if [ -f /var/log/syslog ]
then
    mv /var/log/syslog /var/log/syslog_bak
fi

exec_service_on_target $logger_service restart
sleep 5

if exec_service_on_target named start
then
    echo " -> start of named succeeded."
else
    echo " -> start of named failed."
    echo " -> $test: TEST-FAIL"
    if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
    then
        exec_service_on_target dnsmasq start
    fi
    if [ -f /var/log/syslog_bak ]
    then
        mv /var/log/syslog_bak /var/log/syslog
    fi
    exit
fi

sleep 5

if cat /var/log/syslog | grep "BIND"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> log is not generated."
    echo " -> $test: TEST-FAIL"
fi

if [ "$named_status" = "active" -o "$named_status" = "unknown" ]
then
    exec_service_on_target named start
fi
if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
then
    exec_service_on_target dnsmasq start
fi

if [ -f /var/log/syslog_bak ]
then
    mv /var/log/syslog_bak /var/log/syslog
fi
