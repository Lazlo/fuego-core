#!/bin/sh

#  Run the rndc-confgen command in the chroot environment to find the rndc.key file.

test="chroot_rndc-confgen"

named_status=$(get_service_status named)
dnsmasq_status=$(get_service_status dnsmasq)
exec_service_on_target dnsmasq stop
killall -9 named

if [ -d /var/named/ ]
then
    mv /var/named /var/named_bak
fi

mkdir -p /var/named/chroot/etc/bind
cp -f /etc/bind/* /var/named/chroot/etc/bind/
mkdir -p /var/named/chroot/var/named
mkdir -p /var/named/chroot/var/cache/bind
mkdir -p /var/named/chroot/var/run/named

cp /etc/sysconfig/named /etc/sysconfig/named_bak
cp data/bind9/sysconfig/named /etc/sysconfig/named

if [ -f /etc/bind/rndc.key ]
then
    mv /etc/bind/rndc.key /etc/bind/rndc.key_bak
fi

rndc-confgen -a -k rndckey -t /var/named/chroot
if ls /var/named/chroot/etc/bind/rndc.key
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

rm -rf /var/named
if [ -d /var/named_bak ]
then
    mv /var/named_bak /var/named
fi
mv /etc/sysconfig/named_bak /etc/sysconfig/named
mv /etc/bind/rndc.key_bak /etc/bind/rndc.key
if [ "$named_status" = "active" -o "$named_status" = "unknown" ]
then
    exec_service_on_target named start
fi
if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
then
    exec_service_on_target dnsmasq start
fi
