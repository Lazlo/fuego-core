#!/bin/sh

# Start the named on target.
# Check the IP address.
test="named_dig1"

named_status=$(get_service_status named)
dnsmasq_status=$(get_service_status dnsmasq)

exec_service_on_target dnsmasq stop
exec_service_on_target named stop

if [ ! -f /etc/bind/named.conf ]
then
    touch /etc/bind/named.conf
fi

if [ ! -f /etc/bind/rndc.conf ]
then
    touch /etc/bind/rndc.conf
fi

if [ ! -f /etc/resolv.conf ]
then
    touch /etc/resolv.conf
fi

mv /etc/resolv.conf /etc/resolv.conf_bak
cp data/bind9/resolv.conf /etc/resolv.conf
cp data/bind9/sysconfig/named.nochroot /etc/sysconfig/named

mv /etc/bind/named.conf /etc/bind/named.conf_bak
cp data/bind9/named.conf /etc/bind/named.conf

mv /etc/bind/rndc.conf /etc/bind/rndc.conf_bak
cp data/bind9/rndc.conf /etc/bind/rndc.conf

if [ -d /var/named/ ]
then
    mv /var/named /var/named_bak
fi

mkdir -p /var/named

cp data/bind9/addr.arpa.db /var/named/$tst_bind_file
cp data/bind9/linux_test.com.db data/bind9/linux_test.com.db_bak
sed -i 's/remotehost/'"$remotehost"'/g' data/bind9/linux_test.com.db
cp data/bind9/linux_test.com.db /var/named/linux_test.com.db
if [ ! -f /etc/hosts ]
then
    touch /etc/hosts
fi
mv /etc/hosts /etc/hosts_bak
cp data/bind9/hosts /etc/hosts

sleep 10

exec_service_on_target named start

if dig linux-test.com | grep $remotehost
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target named stop

mv /etc/resolv.conf_bak /etc/resolv.conf
mv /etc/bind/named.conf_bak /etc/bind/named.conf
mv /etc/bind/rndc.conf_bak /etc/bind/rndc.conf
mv data/bind9/linux_test.com.db_bak data/bind9/linux_test.com.db
rm -fr /var/named
if [ -d /var/named_bak ]
then
    mv /var/named_bak /var/named
fi
mv /etc/hosts_bak /etc/hosts
if [ "$named_status" = "active" -o "$named_status" = "unknown" ]
then
    exec_service_on_target named start
fi
if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
then
    exec_service_on_target dnsmasq start
fi
