#!/bin/sh

#  Launch named chroot with the target and check if syslogis exist.

test="chroot_syslog-ng"

named_status=$(get_service_status named)
dnsmasq_status=$(get_service_status dnsmasq)
killall -9 named
exec_service_on_target dnsmasq stop

if [ -d /var/named/ ]
then
    mv /var/named /var/named_bak
fi

exec_service_on_target syslog-ng stop

if [ -f /var/log/syslog ]
then
    mv /var/log/syslog /var/log/syslog_bak
fi

mkdir -p /var/named/chroot/etc/bind
cp -f /etc/bind/* /var/named/chroot/etc/bind/
mkdir -p /var/named/chroot/var/named
mkdir -p /var/named/chroot/var/cache/bind
mkdir -p /var/named/chroot/var/run/named

exec_service_on_target syslog-ng restart

named -t /var/named/chroot

sleep 10

if cat /var/log/syslog | grep "BIND"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
killall -9 named
rm -rf /var/named
if [ -d /var/named_bak ]
then
    mv /var/named_bak /var/named
fi
if [ -f /var/log/syslog_bak ]
then
    mv /var/log/syslog_bak /var/log/syslog
fi
if [ "$named_status" = "active" -o "$named_status" = "unknown" ]
then
    exec_service_on_target named start
fi
if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
then
    exec_service_on_target dnsmasq start
fi
