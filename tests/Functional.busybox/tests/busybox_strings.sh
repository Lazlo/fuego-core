#!/bin/sh

#  The testscript checks the following options of the command strings
#  1) Option none

test="strings"

# string-data should have been created previously
if busybox strings string-data | grep "Hello World"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
