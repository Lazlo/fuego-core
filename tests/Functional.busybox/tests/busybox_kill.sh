#!/bin/sh

#  The testscript checks the following options of the command kill
#  1) Option none

test="kill"

secs=23
sleep $secs & pid=$!
if [ $(ps | grep [s]leep | wc -l) = 1 ]
then
    echo " -> $test: sleep $secs succeeded."
else
    echo " -> $test: sleep $secs failed."
    echo " -> $test: TEST-FAIL"
    exit
fi;

busybox kill -9 $pid
if [ $(ps | grep [s]leep | wc -l) = 0 ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
