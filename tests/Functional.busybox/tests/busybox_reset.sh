#!/bin/sh

#  The testscript checks the following options of the command reset
#  1) Option none

test="reset"

if busybox reset
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
