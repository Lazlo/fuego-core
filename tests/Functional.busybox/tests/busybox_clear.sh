#!/bin/sh

#  The testscript checks the following options of the command clear
#  1) Option none

test="clear"

if busybox clear
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
