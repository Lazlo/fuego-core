#!/bin/sh

#  The testscript checks the following options of the command chgrp
#  1) Option: none

test="chgrp1"

mkdir test_dir
touch test_dir/test1
group=$(id -n -g | cut -b -8)
if [ "$(busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f4 | cut -b -8)" = "$group" ]
then
    echo " -> $test: Group info display succeeded."
else
    echo " -> $test: Group info display failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir
    exit
fi;

chgrp bin ./test_dir/test1

if [ "$(busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f4 | cut -b -8)" = "bin" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir
