#!/bin/sh

#  The testscript checks the following options of the command cat
#  1) Option none

test="cat"

echo "hello" > test1
echo "world" > test2
if [ "$(busybox cat test1 test2 | head -n 1)" = "hello" ] && [ "$(busybox cat test1 test2 | tail -n 1)" = "world" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test1 test2;
