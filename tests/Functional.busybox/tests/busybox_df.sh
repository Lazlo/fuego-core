#!/bin/sh

#  The testscript checks the following options of the command df
#  1) Option none

test="df"

if busybox df | head -n 1 | grep 1K-blocks
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
