#!/bin/sh

#  The testscript checks the following options of the command uname
#  1) Option: -r

test="uname"

if cat /proc/version | grep "$(busybox uname -r)"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
    exit
fi;
