#!/bin/sh

#  The testscript checks the following options of the command ls
#  1) Option: -l

test="ls"

mkdir test_dir
touch test_dir/test1 test_dir/test2 test_dir/test3
busybox ls -l ./test_dir > log

# It makes it easier to diagnose problems if we show the log
cat log

#  Validate the file mode of `ls -l`
# take the second entry, to avoid examining 'total' line
mode=$(cat log | cut -d ' ' -f 1 | head -n 2 | tail -n 1)
if [ "$mode" = "-rw-r--r--" ]
then
    echo " -> $test: File mode is right"
else
    echo "Error: File mode was not as expected. Got: $mode"
    echo " -> $test: TEST-FAIL"
    rm log
    rm -rf test_dir
    exit 1
fi;

#  Validate the filename of `ls -l`
cat log | cut -d ':' -f 2 > filenames
if grep "test1" filenames && grep "test2" filenames && grep "test3" filenames
then
    echo " -> $test: TEST-PASS"
else
    echo "Error: missing expected filename(s) in ls -l output"
    echo " -> $test: TEST-FAIL"
fi;
rm log filenames
rm -rf test_dir
