#!/bin/sh

#  The testscript checks the following options of the command expr
#  1) Option none

test="expr"

if busybox expr 3 \< 5 == 1 && busybox expr 3 \< 2 = 0 && busybox expr length "HELLO WORLD" = 11
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
