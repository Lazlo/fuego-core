#!/bin/sh

#  The testscript checks the following options of the command uptime
#  1) Option none

test="uptime"

if [ "$(busybox uptime | grep ".*average.*")" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
