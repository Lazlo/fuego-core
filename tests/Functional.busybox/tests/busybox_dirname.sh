#!/bin/sh

#  The testscript checks the following options of the command dirname
#  1) Option none

test="dirname"

if [ "$(busybox dirname ./test_dir/test1)" = "./test_dir" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
