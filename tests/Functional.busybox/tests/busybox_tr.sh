#!/bin/sh

#  The testscript checks the following options of the command tr
#  1) Option: none

test="tr"

if [ "$(echo "gdkkn vnqkc" | busybox tr [a-y] [b-z])" = "hello world" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
