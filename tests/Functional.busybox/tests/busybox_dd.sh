#!/bin/sh

#  The testscript checks the following options of the command dd
#  1) Option none

test="dd"

echo "hello world" > test1
touch test2
busybox dd if=test1 of=test2 2>>log

if  tail -n 1 log | grep "0+1\ records\ out" && head -n 1 log | grep "0+1\ records\ in"
then
    echo " -> $test: grep succeeded."
else
    echo " -> $test: grep failed."
    echo " -> $test: TEST-FAIL"
    rm -fr test1 test2
    rm log
    exit
fi;

if [ "$(busybox more test2)" = "hello world" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

rm -fr test1 test2;
rm log;
