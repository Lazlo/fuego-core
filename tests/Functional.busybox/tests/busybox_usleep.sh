#!/bin/sh

#  The testscript checks the following options of the command usleep
#  1) Option none

test="usleep"

if busybox usleep 0 | ps aux | grep "[b]usybox usleep 0"
then
    echo " -> $test: usleep 0 failed."
    echo " -> $test: TEST-FAIL"
    exit
else
    echo " -> $test: usleep 0 succeeded."
fi;

if busybox usleep 1000000 | ps aux | grep "[b]usybox usleep 1000000"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
