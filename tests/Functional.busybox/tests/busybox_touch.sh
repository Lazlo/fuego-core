#!/bin/sh

#  The testscript checks the following options of the command touch
#  1) Option: none

test="touch"

rm -fr test_dir
mkdir test_dir
busybox touch ./test_dir/test1
if [ "$(ls ./test_dir)" = "test1" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr test_dir
