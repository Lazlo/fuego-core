NEED_ROOT=1

function test_pre_check {
    assert_has_program logrotate
    assert_has_program date
}

function test_deploy {
    put $TEST_HOME/logrotate_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    export test_host=$SRV_IP;\
    ./logrotate_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
