#!/bin/sh

#  Turns on verbose mode.
#  option : -v

test="log status add"

if [ -f /var/lib/logrotate.status ]
then
    mv /var/lib/logrotate.status /var/lib/logrotate.status_bak
fi
cp data/test.conf /etc/logrotate.d/test.conf

logrotate -v /etc/logrotate.d/test.conf

if tail /var/lib/logrotate.status | grep testlog
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm -f /etc/logrotate.d/test.conf
if [ -f /var/lib/logrotate.status_bak ]
then
    mv /var/lib/logrotate.status_bak /var/lib/logrotate.status
fi
