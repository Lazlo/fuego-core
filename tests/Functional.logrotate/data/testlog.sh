#!/bin/sh

today=$(date +%s)
# there are 86400 seconds in a day
yesterday=$(( $today - 86400 ))
ddch=$(date --date "@$yesterday" "+%Y-%m-%d")
echo "logrotate state -- version 2" >> /var/lib/logrotate.status
echo "\"/var/log/testlog\" $ddch" >> /var/lib/logrotate.status
