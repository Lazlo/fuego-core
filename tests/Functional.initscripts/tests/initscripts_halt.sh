#!/bin/sh

# In target, verify that the file is executable.
# option: none

test="halt"

if [ -x /etc/init.d/halt ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
