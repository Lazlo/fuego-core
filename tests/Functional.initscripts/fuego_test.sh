function test_pre_check {
    assert_has_program ifup
    assert_has_program ifdown
    assert_has_program ifconfig
}

function test_deploy {
    put $TEST_HOME/initscripts_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    ./initscripts_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
