#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys, collections

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

measurements = {}
measurements = collections.OrderedDict()

regex_string = '^ -> (.*): TEST-(.*)$'
matches = plib.parse_log(regex_string)

if matches:
    for m in matches:
        tc_name="default."+ m[0]
        tc_result = m[1]
        if tc_result in ["PASS", "SKIP", "FAIL"]:
            measurements[tc_name] = tc_result
        else:
            measurements[tc_name] = "ERROR"

# split the output for each testcase
plib.split_output_per_testcase(regex_string, measurements)

sys.exit(plib.process(measurements))
