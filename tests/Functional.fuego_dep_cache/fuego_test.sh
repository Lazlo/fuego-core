
function test_run {
    log_this "echo Starting dependency cache checks"

    # some commands may fail
    set +e

    # now do some host-driven direct tests of assert_has_program

    # first, clear the program cache
    ftc delete-var -b $NODE_NAME PROGRAM_LS
    ftc delete-var -b $NODE_NAME PROGRAM_LL
    unset CACHE_DEPENDENCIES

    desc="check for ls using old system"
    start_time=$(date +"%s.%N")
    assert_has_program_slow ls
    end_time=$(date +"%s.%N")
    duration=$(python -c "print $end_time - $start_time")

    log_this "echo \"# after assert_has_program PROGRAM_LS=$PROGRAM_LS\""
    log_this "echo \"# assert duration=$duration\""
    if [ "$PROGRAM_LS" = "/bin/ls" ] ; then
        log_this "echo ok 1 - $desc"
    else
        log_this "echo not ok 1 - $desc"
    fi

    unset PROGRAM_LS
    log_this "echo \"# after unset, PROGRAM_LS=$PROGRAM_LS\""

    desc="check for ls using new system"
    start_time=$(date +"%s.%N")
    assert_has_program ls
    end_time=$(date +"%s.%N")
    duration=$(python -c "print $end_time - $start_time")
    log_this "echo \"# PROGRAM_LS=$PROGRAM_LS\""
    log_this "echo \"# assert duration=$duration\""
    if [ "$PROGRAM_LS" = "/bin/ls" ] ; then
        log_this "echo ok 2 - $desc"
    else
        log_this "echo not ok 2 - $desc"
    fi

    desc="check if 'ls' is in the cache, expecting no"
    cache_var=$(ftc query-board -b $NODE_NAME -n PROGRAM_LS 2>/dev/null)
    log_this "echo \"# cache_var PROGRAM_LS=$cache_var\""
    if [ "$cache_var" = "" ] ; then
        log_this "echo ok 3 - $desc"
    else
        log_this "echo not ok 3 - $desc"
    fi

    unset PROGRAM_LS
    log_this "echo \"# after unset, PROGRAM_LS=$PROGRAM_LS\""

    export CACHE_DEPENDENCIES=1
    desc="check for ls using new system, with caching"
    start_time=$(date +"%s.%N")
    assert_has_program ls
    end_time=$(date +"%s.%N")
    duration=$(python -c "print $end_time - $start_time")
    log_this "echo \"# CACHE_DEPENDENCIES=$CACHE_DEPENDENCIES\""
    log_this "echo \"# after assert_has_program PROGRAM_LS=$PROGRAM_LS\""
    log_this "echo \"# assert duration=$duration\""
    if [ "$PROGRAM_LS" = "/bin/ls" ] ; then
        log_this "echo ok 4 - $desc"
    else
        log_this "echo not ok 4 - $desc"
    fi

    desc="check if 'ls' is in the cache, expecting yes"
    cache_var=$(ftc query-board -b $NODE_NAME -n PROGRAM_LS 2>/dev/null)
    log_this "echo \"# cache_var PROGRAM_LS=$cache_var\""
    if [ "$cache_var" = "/bin/ls" ] ; then
        log_this "echo ok 5 - $desc"
    else
        log_this "echo not ok 5 - $desc"
    fi

    desc="check for ls using new system, with data in cache"
    start_time=$(date +"%s.%N")
    assert_has_program ls
    end_time=$(date +"%s.%N")
    duration=$(python -c "print $end_time - $start_time")
    log_this "echo \"# PROGRAM_LS=$PROGRAM_LS\""
    log_this "echo \"# assert duration=$duration\""
    if [ "$PROGRAM_LS" = "/bin/ls" ] ; then
        log_this "echo ok 6 - $desc"
    else
        log_this "echo not ok 6 - $desc"
    fi

    set -x
    cmd "command -v ll"
    cmd "command -v alias"
    set +x

    desc="check for ll alias using new system"
    start_time=$(date +"%s.%N")
    check_has_program ll
    end_time=$(date +"%s.%N")
    duration=$(python -c "print $end_time - $start_time")
    log_this "echo \"# PROGRAM_LL=$PROGRAM_LL\""
    log_this "echo \"# check duration=$duration\""
    if [ -z "$PROGRAM_LL" ] ; then
        log_this "echo ok 7 - $desc"
    else
        log_this "echo not ok 7 - $desc"
    fi

    desc="check for egrep alias using new system"
    start_time=$(date +"%s.%N")
    check_has_program egrep
    end_time=$(date +"%s.%N")
    duration=$(python -c "print $end_time - $start_time")
    log_this "echo \"# PROGRAM_EGREP=$PROGRAM_EGREP\""
    log_this "echo \"# check duration=$duration\""
    if [ "$PROGRAM_EGREP" = "/bin/egrep" ] ; then
        log_this "echo ok 8 - $desc"
    else
        log_this "echo not ok 8 - $desc"
    fi
}
