#!/bin/sh

#  In the target start ospfd and zebra, then confirm the process condition by /var/run/quagga/ospfd.pid file.
#  check the keyword "ospfd".

test="pidfile"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target ospfd stop
exec_service_on_target zebra stop
rm -f /var/run/quagga/ospfd.pid

#Backup the config file
mv /etc/quagga/ospfd.conf /etc/quagga/ospfd.conf.bck
mv /etc/quagga/zebra.conf /etc/quagga/zebra.conf.bck

cp data/ospfd.conf /etc/quagga/ospfd.conf
cp data/zebra.conf /etc/quagga/zebra.conf
chown quagga:quagga /etc/quagga/*.conf

if exec_service_on_target zebra start
then
    echo " -> start of zebra succeeded."
else
    echo " -> start of zebra failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target ospfd start
then
    echo " -> start of ospfd succeeded."
else
    echo " -> start of ospfd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ls /var/run/quagga/ospfd.pid
then
    echo " -> get the pid of ospfd."
else
    echo " -> can't get the pid of ospfd."
    echo " -> $test: TEST-FAIL"
    exit
fi

exec_service_on_target ospfd stop
exec_service_on_target zebra stop

if ls /var/run/quagga/ospfd.pid
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

#Restore the config file
mv /etc/quagga/ospfd.conf.bck /etc/quagga/ospfd.conf
mv /etc/quagga/zebra.conf.bck /etc/quagga/zebra.conf
