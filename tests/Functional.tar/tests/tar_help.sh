#!/bin/sh

#  In target, run command tar.
#  option: --help

test="help"

if tar --help | grep Usage
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
