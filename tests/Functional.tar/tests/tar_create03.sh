#!/bin/sh

#  In target, run command tar.
#  option: cjf(bzip2)

test="create03"

mkdir test_dir

if tar cf mytest.tar.bz2 test_dir
then
    echo "creation of tar succeeded."
else
    echo "creation of tar failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if test -f mytest.tar.bz2
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr test_dir
