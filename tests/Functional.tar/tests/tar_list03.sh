#!/bin/sh

#  In target, run command tar.
#  option: tjf(bzip2)

test="list03"

if tar tjf data/test.tar.bz2 | grep "test_c.txt"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
