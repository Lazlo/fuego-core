service_name="service_name-not-set"
config_file="config_file-not-set"
pid_file="pid_file-not-set"
module_status="unknown"

#set the variable of service openvpn
set_service_file() {
    if [ "$init_manager" == "systemd" ]
    then
        service_name="openvpn@loopback-server.service"
        config_file="/etc/openvpn/loopback-server.conf"
        pid_file="/var/run/openvpn/loopback-server.pid"
    else
        service_name="openvpn"
        if [ "$test_type" == "client" ]
        then
                config_file="/etc/openvpn/testcli.conf"
                pid_file="/var/run/openvpn.testcli.pid"
        elif [ "$test_type" == "server" ]
        then
                config_file="/etc/openvpn/testsrv.conf"
                pid_file="/var/run/openvpn.testsrv.pid"
        fi
    fi
}

setup_routine() {
    set_service_file
    service_status=$(get_service_status $service_name)
    module_status=$(get_module_status tun)
    exec_service_on_target $service_name stop

    if [ -f $config_file ]
    then
        mv $config_file "${config_file}_bak"
    fi

    test_type=$1
    if [ "$test_type" = "client" ]
    then
        cp data/testcli.conf $config_file
    elif [ "$test_type" = "server" ] ; then
        cp data/testsrv.conf $config_file
    fi

    if [ -f /etc/openvpn/host-target.key ]
    then
        mv /etc/openvpn/host-target.key /etc/openvpn/host-target.key_bak
    fi
    cp data/host-target.key /etc/openvpn/host-target.key

    if [ -d /dev/net ]
    then
        mv /dev/net /dev/net_bak
    fi
    mkdir -m 755 /dev/net
    mknod /dev/net/tun c 10 200
}

restore_routine() {
    if [ -d /dev/net_bak ]
    then
        mv /dev/net_bak /dev/net
    fi
    rm $config_file
    if [ -f "${config_file}_bak" ]
    then
        mv "${config_file}_bak" $config_file
    fi
    rm -r /etc/openvpn/host-target.key
    if [ -f /etc/openvpn/host-target.key_bak ]
    then
        mv /etc/openvpn/host-target.key_bak /etc/openvpn/host-target.key
    fi
    if [ "$module_status" = "unloaded" ]
    then
        modprobe -r tun
    fi
}
