#!/bin/sh

#  In the target, run openvpn and check the pidfile.

test="pid_$1"

test_type=$1

setup_routine $test_type

if exec_service_on_target $service_name start
then
    echo " -> $test: service start succeeded."
else
    echo " -> $test: service start failed."
    echo " -> $test: TEST-FAIL"
    restore_routine
    exit
fi

if [ -f $pid_file ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
if [ "$service_status" = "inactive" ]
then
    exec_service_on_target $service_name stop
fi
restore_routine
