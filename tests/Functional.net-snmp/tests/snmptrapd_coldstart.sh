#!/bin/sh

#  In the target start snmptrapd, and check the syslog.
#  check the keyword "SNMPv2-MIB::coldStart".

test="snmptrapd_coldstart.sh"

snmptrapd_status=$(get_service_status snmptrapd)
snmptrapd_logfile=$(get_service_logfile)
logger_service=$(detect_logger_service)

exec_service_on_target snmptrapd stop
exec_service_on_target $logger_service stop

mv /etc/snmp/snmptrapd.conf /etc/snmp/snmptrapd.conf_bak
cp data/net-snmp/snmptrapd.conf /etc/snmp/snmptrapd.conf

restore_target() {
    mv /etc/snmp/snmptrapd.conf_bak /etc/snmp/snmptrapd.conf
    if [ -f $snmptrapd_logfile"_bak" ]
    then
        mv $snmptrapd_logfile"_bak" $snmptrapd_logfile
    fi
}

if [ -f $snmptrapd_logfile ]
then
    mv $snmptrapd_logfile $snmptrapd_logfile"_bak"
fi

exec_service_on_target $logger_service restart

sleep 2

if exec_service_on_target snmptrapd start
then
    echo " -> start of snmptrapd succeeded."
else
    echo " -> start of snmptrapd failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

snmptrap -v2c -c "" -M /usr/share/snmp/mibs localhost "" SNMPv2-MIB::coldStart

sleep 3

if cat $snmptrapd_logfile | grep "SNMPv2-MIB::coldStart"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ "$snmptrapd_status" = "inactive" ]
then
    exec_service_on_target snmptrapd stop
fi
restore_target
