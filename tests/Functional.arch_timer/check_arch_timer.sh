#!/bin/sh
# SPDX-License-Identifier: MIT
#
# check_arch_timer.sh - checks for presence and operation of
#   arch-specific timer interrupt for a board
#
# Usage: check_arch_time.sh <int_name> <int_id> [<int_string>]
#
# Output is in TAP13 format
#
# Copyright 2018 Sony Corporation
#
# Author: Tim Bird <tim.bird (at) sony.com>
#

usage() {
    cat <<HERE
Usage: check_arch_timer.sh [-h] <int_name> <int_id> [<int_string>]

Arguments:
 -h  = show this usage help

int_name is the name of the interrupt in /proc/interrupts
int_id is the number of the interrupt in /proc/interupts
 (on some systems, this may be a non-numeric identifier)
int_string is an optional string pattern used to identify the initialization
  message for the interrupt in kernel message (dmesg output)
HERE
   exit 0
}

# $1 = line with interrupt counts
# $2 = cpu number (optional, defaults to 0)
# returns the current count for the indicated cpu number from the line
int_count() {
    line=$1

    pos=$2
    if [ -z "$pos" ] ; then
        pos=0
    fi

    # remove leading interrupt number (or id)
    nums="${line#*: }"

    # split on spaces
    SAVE_IFS="$IFS"
    IFS=" "
    set -- $nums
    while [ $pos -gt 0 ] ; do
        shift
        pos=$(( $pos - 1 ))
    done
    count=$1
    IFS="$SAVE_IFS"
    echo $count
}

## parse arguments
IRQ_NAME="$1"
IRQ_ID="$2"

if [ -z "$IRQ_ID" ] ; then
    usage
fi
if [ "$IRQ_NAME" = "-h" ] ; then
    usage
fi
IRQ_STRING="$3"

echo "TAP version 13"

echo "Test for arch_timer attributes"
echo "IRQ_NAME=\"$IRQ_NAME\""
echo "IRQ_ID=\"$IRQ_ID\""

tap_id=1
desc="${tap_id} Check interrupt name in proc filesystem"
if grep "$IRQ_NAME" /proc/interrupts ; then
    echo "ok ${desc}"
else
    echo "  Did not find $IRQ_NAME in /proc/interrupts"
    echo "not ok ${desc}"
fi

tap_id=$(( $tap_id + 1 ))
desc="${tap_id} Check interrupt number in proc filesystem"
if grep "$IRQ_NAME" /proc/interrupts | grep "$IRQ_ID" ; then
    echo "ok ${desc}"
else
    echo "  Did not find $IRQ_ID for $IRQ_NAME in /proc/interrupts"
    echo "not ok ${desc}"
fi

# now read interrupt counts, with an interval in between
start_line=$(grep "$IRQ_NAME" /proc/interrupts)
if [ -z "$start_line" ] ; then
    echo "Error reading interrupt '$1' from /proc/interrupts"
fi
sleep 5
end_line=$(grep "$IRQ_NAME" /proc/interrupts)
# no need to check presence again

num_cpus=$(grep "processor" /proc/cpuinfo | wc -l)

# for each cpu, parse count from lines, and compare
cpu_num=0
while [ $cpu_num -lt $num_cpus ] ; do
    tap_id=$(( $tap_id + 1 ))
    desc="${tap_id} Check interrupt count for CPU $cpu_num is increasing"
    start_count=$(int_count "$start_line" $cpu_num)
    end_count=$(int_count "$end_line" $cpu_num)

    echo "  start_count=${start_count}"
    echo "  end_count=${end_count}"

    if [ -z "$start_count" -o -z "$end_count" ] ; then
        echo "  Problem parsing count from interrupt line"
        echo "not ok ${desc}"
        break
    fi

    if [ $start_count -lt $end_count ] ; then
        echo "ok ${desc}"
    else
        echo "  Count for interrupt $IRQ_NAME is not increasing"
        echo "not ok ${desc}"
    fi
    cpu_num=$(( $cpu_num + 1 ))
done

if [ -n "$IRQ_STRING" ] ; then
    tap_id=$(( $tap_id + 1 ))
    desc="${tap_id} Check interrupt initialization string in dmesg"
    if dmesg | grep "$IRQ_STRING" ; then
        echo "ok ${desc}"
    else
        echo "  Did not find '$IRQ_STRING' in dmesg output"
        echo "not ok ${desc}"
    fi
fi

echo "1..${tap_id}"
