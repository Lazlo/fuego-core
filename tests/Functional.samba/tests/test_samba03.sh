#!/bin/sh

#  In the target start samba, and check if the logfile of samba is exist.

test="samba03"

service_status=$(get_service_status smb)
exec_service_on_target smb stop

if [ -e /var/log/samba/log.smbd ]
then
    mv /var/log/samba/log.smbd /var/log/samba/log.smbd_bak
fi

restore_target(){
    if [ -f /var/log/samba/log.smbd_bak ]
    then
        mv /var/log/samba/log.smbd_bak /var/log/samba/log.smbd
    fi
}

if exec_service_on_target smb start
then
    echo " -> start smb succeeded."
else
    echo " -> start smb failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

sleep 5

if test -f /var/log/samba/log.smbd
then
    echo " -> /var/log/samba/log.smbd exists."
    echo " -> $test: TEST-PASS"
else
    echo " -> /var/log/samba/log.smbd dose not exist."
    echo " -> $test: TEST-FAIL"
fi

if [ "$service_status" = "inactive" ]
then
    exec_service_on_target smb stop
fi
restore_target
