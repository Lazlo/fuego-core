#!/bin/sh

#  In the target start samba, and check if the /var/run/smb.pid is exist.

test="samba02"

service_status=$(get_service_status smb)
exec_service_on_target smb stop

if exec_service_on_target smb start
then
    echo " -> start smb succeeded."
else
    echo " -> start smb failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5

if test -f /var/run/smbd.pid
then
    echo " -> /var/run/smbd.pid is exist."
else
    echo " -> /var/run/smbd.pid is not exist."
    echo " -> $test: TEST-FAIL"
    if [ "$service_status" = "inactive" ]
    then
        exec_service_on_target smb stop
    fi
    exit
fi

if exec_service_on_target smb stop
then
    echo " -> stop smb succeeded."
else
    echo " -> stop smb failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5

if test ! -f /var/run/smbd.pid
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target smb start
fi
