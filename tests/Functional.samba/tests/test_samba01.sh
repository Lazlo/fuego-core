#!/bin/sh

#  In the target start samba, and confirm the process condition by command ps.
#  check the keyword "smbd".

test="samba01"

service_status=$(get_service_status smb)

exec_service_on_target smb stop

if exec_service_on_target smb start
then
    echo " -> start of smb succeeded."
else
    echo " -> start of smb failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5

if ps aux | grep "[/]usr/sbin/smbd"
then
    echo " -> get the pid of smbd."
else
    echo " -> can't get the pid of smbd."
    echo " -> $test: TEST-FAIL"
    if [ "$service_status" = "inactive" ]
    then
        exec_service_on_target smb stop
    fi
    exit
fi

exec_service_on_target smb stop

if ps aux | grep "[/]usr/sbin/smbd"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target smb start
fi
