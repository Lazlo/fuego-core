#!/bin/sh

# Access the samba server on the target from the host

test="samba06"
expect <<-EOF
spawn sh
expect ".*"
send "smbclient //$1/test -N\r"
expect {
 -re ".*smb:.*" {
           send "ls\r"
          }
 default { send_user "Can't connect to the board"\n"}  }
expect {
 -re ".*test1.*" {
           send_user " -> $test: TEST-PASS\n"
          }
 default { send_user " -> $test: TEST-FAIL\n"}  }
send "exit\n"
expect eof
EOF
