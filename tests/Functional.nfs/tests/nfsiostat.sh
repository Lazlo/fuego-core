#!/bin/sh

#  In the target run the command nfiosstat, and displays NFS client
#  per-mount statisitics..
#  option : -a, -d, -h, -p, -s, --version

test="nfsiostat"

if nfsiostat -a | grep "attribute cache"
then
    echo " -> Display statistics related to the attribute cache succeeded."
else
    echo " -> Display statistics related to the attribute cache failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if nfsiostat -d | grep "lookup"
then
    echo " -> Display statistics related to directory operations succeeded."
else
    echo " -> Display statistics related to directory operations failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if nfsiostat -h | grep "Usage"
then
    echo " -> Show help message succeeded."
else
    echo " -> Show help message failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if nfsiostat -p | grep "page"
then
    echo " -> Display statistics related to the page cache succeeded."
else
    echo " -> Display statistics related to the page cache failed."
    echo " -> $test: TEST-FAIL"
    exit
fi


if nfsiostat -s | grep "ops"
then
    echo " -> Sort NFS mount points by ops/second succeeded."
else
    echo " -> Sort NFS mount points by ops/second failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if nfsiostat --version | grep "version"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> Show version number failed."
    echo " -> $test: TEST-FAIL"
fi
