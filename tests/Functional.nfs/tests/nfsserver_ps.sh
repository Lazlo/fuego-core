#!/bin/sh

#  In the target start nfsserver, and confirm the process condition
#  using the ps command.

test="nfsserver_ps"

. ./fuego_board_function_lib.sh

set_init_manager

if [ "$init_manager" = "systemd" ]
then
    service_name="nfs-server"
else
    service_name="nfsserver"
fi

nfs_status=$(get_service_status $service_name)

rpcbind_status=$(get_service_status rpcbind)

exec_service_on_target $service_name stop
exec_service_on_target rpcbind stop

if [ -f /etc/exports ]
then
    mv /etc/exports /etc/exports_bak
fi

touch /etc/exports

restore_target() {
    if [ -f /etc/exports_bak ]
    then
        mv /etc/exports_bak /etc/exports
    else
        rm -f /etc/exports
    fi
}

exec_service_on_target rpcbind start
if exec_service_on_target $service_name start
then
    echo " -> start of nfsserver succeeded."
else
    echo " -> start of nfsserver failed."
    echo " -> $test: TEST-FAIL"
    if [ "$rpcbind_status" = "inactive" ]
    then
        exec_service_on_target rpcbind stop
    fi
    restore_target
    exit
fi

sleep 5

if ps aux | grep "[\[]nfsd"
then
    echo " -> nfsd process is running."
else
    echo " -> nfsd process is not running."
    echo " -> $test: TEST-FAIL"
    if [ "$rpcbind_status" = "inactive" ]
    then
        exec_service_on_target rpcbind stop
    fi
    if [ "$nfs_status" = "inactive" ]
    then
        exec_service_on_target $service_name stop
    fi
    restore_target
    exit
fi

exec_service_on_target rpcbind stop
exec_service_on_target $service_name stop

sleep 5

if ps aux | grep "[\[]nfsd"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi
restore_target
if [ "$rpcbind_status" = "active" -o "$rpcbind_status" = "unknown" ]
then
    exec_service_on_target rpcbind start
fi

if [ "$nfs_status" = "active" -o "$nfs_status" = "unknown" ]
then
    exec_service_on_target $service_name start
fi
