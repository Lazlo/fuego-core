#!/usr/bin/python

import os, sys, collections
import common as plib
import json

# allocate variable to store the results
measurements = {}
measurements = collections.OrderedDict()

# read results from linaro result.json format
with open(plib.LOGDIR + "/result.json") as f:
    data = json.load(f)[0]

for test_case in data['metrics']:
    test_case_id = test_case['test_case_id']
    result = test_case['result']
    # FIXTHIS: add measurements when available
    # measurement = test_case['measurement']
    # units = test_case['units']
    measurements['default.' + test_case_id] = result.upper()

# FIXTHIS: think about how to get each test's log from stdout.log

sys.exit(plib.process(measurements))
